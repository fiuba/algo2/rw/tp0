# TP0

Deben editar el archivo tp0.c, completando las cuatro funciones que aparecen en el código fuente: swap(), maximo(), comparar() y seleccion(). (Los comentarios en el archivo indican qué debe hacer cada función).

El resto de archivos no los deben modificar. Sí pueden, no obstante, leer el archivo tp0_pruebas.c para entender qué verificaciones realizaremos sobre su código.

La compilación se realiza en el estándar C99, con los siguientes flags:

'''gcc -std=c99 -Wall -Wconversion -Wtype-limits -pedantic -Werror -o pruebas *.c'''

Las pruebas deben dar todas OK, y las pueden ejecutar con:

'''./pruebas'''

En la entrega de ser impresa en una hoja de papel, con su nombre y padrón, imprimiendo únicamente el archivo tp0.c.

Además, deben subir el código a la página de entregas de la materia, con el código completo.
